"""Master URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

import registrasi.urls as registrasi
import coin.urls as coin
import authentication.urls as authentication
import produk.urls as produk
import produksi.urls as produksi
import histori_produk_makanan.urls as histori_produk_makanan
import lumbung.urls as lumbung
import histori_tanaman.urls as histori_produk_tanaman
import transaksi_upgrade_lumbung.urls as transaksi_upgrade_lumbung
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(registrasi)),
    path('coin/', include(coin)),
    path('authentication/', include(authentication)),
    path('produk/', include(produk)),
    path('produksi/', include(produksi)),
    path('histori_produksi_makanan/', include(histori_produk_makanan)),
    path('lumbung/', include(lumbung)),
    path('histori_produk_tanaman/',include(histori_produk_tanaman)),
    path('transaksi_upgrade_lumbung/',include(transaksi_upgrade_lumbung))
]
