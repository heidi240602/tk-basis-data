
from django.urls import path
from .views import upgrade_lumbung, daftar_transaksi_upgrade_lumbung

urlpatterns = [
    path('daftar_transaksi_upgrade_lumbung',daftar_transaksi_upgrade_lumbung,name='daftar_upgrade_lumbung'),
    path('upgrade_lumbung',upgrade_lumbung,name='upgrade_lumbung')
]