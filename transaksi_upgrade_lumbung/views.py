from django.shortcuts import render
import datetime
from time import strftime
from django.shortcuts import redirect, render
from django.db import connection
from .forms import CreateTransaksiUpgradeLumbung

def daftar_transaksi_upgrade_lumbung(request):
    if('email' in request.session)!=True:
        return redirect ('authentication/user_login')
    if request.session['role']!= 'pengguna':
        response={}
        sql_command="SET SEARCH_PATH TO HI_DAY"
        sql_command2="SELECT * FROM TRANSAKSI_UPGRADE_LUMBUNG"
        with connection.cursor() as cursor:
            cursor.execute(sql_command)
            cursor.execute(sql_command2)
            response["daftar_transaksi_upgrade_lumbung"]=cursor.fetchall()
        return render(request,'daftar_transaksi_upgrade_lumbung_admin.html',response)

    response={}
    email="'"+request.session['email']+"'"
    sql_command="SET SEARCH_PATH TO HI_DAY"
    sql_command2="SELECT * FROM TRANSAKSI_UPGRADE_LUMBUNG WHERE EMAIL="+email
    with connection.cursor() as cursor:
        cursor.execute(sql_command)
        cursor.execute(sql_command2)
        response["daftar_transaksi_upgrade_lumbung"]=cursor.fetchall()
    return render(request,'daftar_transaksi_upgrade_lumbung.html',response)

def upgrade_lumbung(request):
    response={}
    response['error message']=""
    if('email' in request.session)!=True:
        return redirect ('authentication/user_login')
    if request.session['role']!='pengguna':
        return redirect('/daftar_transaksi_upgrade_lumbung')

    email=request.session['email']
    lumbung=cari_lumbung(email)[0]
    print(lumbung)
    response['email_pemilik']=lumbung[0]
    response['level']=lumbung[1]
    response['level_update']=lumbung[1]+1
    response['kapasitas_maksimal']=lumbung[2]
    response['kapasitas_update']=lumbung[2]+50
    if request.method == 'POST':
        form=CreateTransaksiUpgradeLumbung(request.POST)
        if form.is_valid():
            return redirect('upgrade_lumbung/daftar_transaksi_upgrade_lumbung')
        else:
            email="'"+request.session['email']+"'"
            waktu="'"+str(datetime.datetime.now())+"'"
            sql_command='SET SEARCH_PATH TO HI_DAY'
            sql_command2='UPDATE LUMBUNG SET LEVEL=LEVEL+1, KAPASITAS_MAKSIMAL=KAPASITAS_MAKSIMAL+50 WHERE EMAIL={}'.format(email)
            sql_command3='INSERT INTO TRANSAKSI_UPGRADE_LUMBUNG (EMAIL,WAKTU) VALUES({},{})'.format(email,waktu)
            pengguna=cari_pengguna(email)[0]
            if pengguna[4]<200:
                response["error message"]="Koin anda tidak cukup, silahkan cari Koin terlebih dahulu"
                return render(request,'upgrade_lumbung.html',context=response)
            with connection.cursor() as cursor:
                cursor.execute(sql_command)
                cursor.execute(sql_command3)
                cursor.execute(sql_command2)
                
            return redirect('/daftar_transaksi_upgrade_lumbung')
    form=CreateTransaksiUpgradeLumbung(initial={'level_lumbung':response['level'],'kapasitas_lumbung':response['kapasitas_maksimal']})
    response['transaksi_upgrade_lumbung']=form
    return render(request,'upgrade_lumbung.html',response)
    

def cari_lumbung(email):
    with connection.cursor() as cursor:
        cursor.execute('SET SEARCH_PATH TO HI_DAY')
        cursor.execute('SELECT * FROM LUMBUNG WHERE EMAIL ='+"'"+str(email)+"'")     
        data=cursor.fetchall()
    return data

def cari_pengguna(email):
    with connection.cursor() as cursor:
        cursor.execute('SET SEARCH_PATH TO HI_DAY')
        cursor.execute('SELECT * FROM PENGGUNA WHERE EMAIL ='+str(email))     
        data=cursor.fetchall()
    return data