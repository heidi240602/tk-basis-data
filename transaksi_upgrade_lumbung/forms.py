from django import forms

class CreateTransaksiUpgradeLumbung(forms.Form):
    level_lumbung=forms.IntegerField(
        label='level lumbung',
        required=True
    )
    kapasitas_lumbung=forms.IntegerField(
        label='kapasitas lumbung',
        required=True
    )