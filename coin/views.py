import datetime
from time import strftime
from django.shortcuts import redirect, render
from django.db import connection
from .forms import CreatePaketKoinForm
from .forms import UpdatePaketKoinForm 
from .forms import DeletePaketKoinForm
from .forms import CreateTransaksiPaketKoinForm


def list_paket_koin(request):
    if ('email' in request.session)!=True:
        return redirect('authentication/user_login')
    if request.session['role']=='admin':
        response={}
        sql_command="SET SEARCH_PATH TO HI_DAY"
        sql_command_execute="SELECT * FROM PAKET_KOIN "
        with connection.cursor() as cursor:
            cursor.execute(sql_command)
            cursor.execute(sql_command_execute)
            response["daftar_paket_koin"]=cursor.fetchall()
        return render(request, "list_paket_koin_admin.html",response)
    response={}
    sql_command="SET SEARCH_PATH TO HI_DAY"
    sql_command_execute="SELECT * FROM PAKET_KOIN "
    with connection.cursor() as cursor:
        cursor.execute(sql_command)
        cursor.execute(sql_command_execute)
        response["daftar_paket_koin"]=cursor.fetchall()
    return render(request, "list_paket_koin.html",response)

def list_transaksi_paket_koin(request):
    if ('email' in request.session)!=True:
        return redirect('authentication/user_login')
    response={}
    if request.session['role']=='pengguna':
        sql_command="SET SEARCH_PATH TO HI_DAY"
        sql_command_execute="SELECT * FROM TRANSAKSI_PEMBELIAN_KOIN WHERE EMAIL="+"'"+request.session['email']+"'"
        with connection.cursor() as cursor:
            cursor.execute(sql_command)
            cursor.execute(sql_command_execute)
            response["daftar_transaksi_paket_koin"]=cursor.fetchall()
        return render(request,"list_transaksi_paket_koin.html",response)
    sql_command="SET SEARCH_PATH TO HI_DAY"
    sql_command_execute="SELECT * FROM TRANSAKSI_PEMBELIAN_KOIN"
    with connection.cursor() as cursor:
        cursor.execute(sql_command)
        cursor.execute(sql_command_execute)
        response["daftar_transaksi_paket_koin"]=cursor.fetchall()
    return render(request,"list_transaksi_paket_koin_admin.html",response)

def create_paket_koin(request):
    # if request.session.get("role", None) == None:
    #     return redirect("/authentication")
    if ('email' in request.session) != True:
        print(request.session)
        return redirect('/authentication/user_login')

    if request.session['role']!="admin":
        print(request.session['role'])
        return redirect('/coin/list_paket_koin')

    if request.method == 'POST':
        form = CreatePaketKoinForm(request.POST)
        if form.is_valid():
            nama_paket=request.POST['paket_koin']
            harga_paket=request.POST['harga']
            sql_command="SET SEARCH_PATH TO HI_DAY"
            sql_command2="INSERT INTO PAKET_KOIN (JUMLAH_KOIN, HARGA) VALUES({}, {})".format(str(nama_paket),str(harga_paket))
            with connection.cursor() as cursor:
                cursor.execute(sql_command)
                cursor.execute(sql_command2)
            return redirect('/coin/list_paket_koin')
    form =CreatePaketKoinForm()
    response = {"create_paket_koin_form": form}
    return render(request, "create_paket_koin.html", response)

def delete_paket_koin(request,paket_koin):
    if ('email' in request.session) != True:
        print(request.session)
        return redirect('/authentication/user_login')

    if request.session['role']!="admin":
        print(request.session['role'])
        return redirect('/coin/list_paket_koin')
    if request.method =='POST':
        form=DeletePaketKoinForm(request.POST)
        data=request.POST.copy()
        if form.is_valid():
            sql_command="SET SEARCH_PATH TO HI_DAY"
            return redirect('/coin/list_paket_koin')
        else:
            data['paket_koin']=paket_koin
            sql_command="SET SEARCH_PATH TO HI_DAY"
            sql_command2="DELETE FROM PAKET_KOIN WHERE JUMLAH_KOIN={}".format(str(data['paket_koin']))
            with connection.cursor() as cursor:
                cursor.execute(sql_command)
                cursor.execute(sql_command2)
            return redirect('/coin/list_paket_koin')
    else:
        form=DeletePaketKoinForm(initial={'paket_koin':paket_koin})
        response={"delete_paket_koin_form":form}
        return render(request,"delete_paket_koin.html",response)

def update_paket_koin(request,paket_koin):
    if ('email' in request.session) != True:
        print(request.session)
        return redirect('/authentication/user_login')

    if request.session['role']!="admin":
        print(request.session['role'])
        return redirect('/coin/list_paket_koin')

    if request.method=='POST':
        form=UpdatePaketKoinForm(request.POST)
        data=request.POST.copy()
        if form.is_valid():
            sql_command="SET SEARCH_PATH TO HI_DAY"
            return redirect('/coin/list_paket_koin')
        else:
            data['paket_koin']=paket_koin
            sql_command="SET SEARCH_PATH TO HI_DAY"
            sql_command2="UPDATE PAKET_KOIN SET HARGA={} WHERE JUMLAH_KOIN={}".format(str(data['harga_baru']), str(data['paket_koin']))
            with connection.cursor() as cursor:
                cursor.execute(sql_command)
                cursor.execute(sql_command2)
            return redirect('/coin/list_paket_koin')
    else:
        form=UpdatePaketKoinForm(initial={'paket_koin':paket_koin})
        response={"update_paket_koin_form":form}
        return render(request,"update_paket_koin.html",response)

def transaksi_paket_koin(request, paket_koin, harga_paket):
    if ('email' in request.session) != True:
        print(request.session)
        return redirect('/authentication/user_login')

    if request.session['role']=="admin":
        return redirect('/coin/list_paket_koin')

    if request.method == 'POST':
        form = CreateTransaksiPaketKoinForm(request.POST)
        data=request.POST.copy()
        if form.is_valid():
            sql_command="SET SEARCH_PATH TO HI_DAY"
            nama_paket=request.POST['paket_koin']
            jumlah=request.POST['jumlah']
            print(nama_paket +" "+jumlah)
            return redirect('/coin/list_transaksi_paket_koin')
        else:
            jumlah=data['jumlah']
            cara_pembayaran="'"+data['cara_pembayaran']+"'"
            waktu="'"+str(datetime.datetime.now())+"'"
            pembeli="'"+request.session['email']+"'"
            total=int(int(jumlah) * int(harga_paket))
            sql_command="SET SEARCH_PATH TO HI_DAY"
            sql_command2="INSERT INTO TRANSAKSI_PEMBELIAN_KOIN (EMAIL, WAKTU, JUMLAH, CARA_PEMBAYARAN, PAKET_KOIN, TOTAL_BIAYA) VALUES({}, {}, {}, {}, {},{})".format(pembeli, str(waktu), str(jumlah), cara_pembayaran, str(paket_koin), str(total))
            with connection.cursor() as cursor:
                cursor.execute(sql_command)
                cursor.execute(sql_command2)
            return redirect('/coin/list_transaksi_paket_koin')
    else:
        form =CreateTransaksiPaketKoinForm(initial={'paket_koin':paket_koin,'harga_paket':harga_paket})
        response= {"create_transaksi_paket_koin_form":form,"name_paket":paket_koin}
        return render(request,"create_transaksi_paket_koin.html",response)

