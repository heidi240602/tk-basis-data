from django.urls import path
from .views import list_paket_koin,list_transaksi_paket_koin,create_paket_koin,transaksi_paket_koin,delete_paket_koin,update_paket_koin

urlpatterns = [
    path('list_paket_koin',list_paket_koin, name='paket_koin'),
    path('list_transaksi_paket_koin',list_transaksi_paket_koin, name='transaksi'),
    path('create_paket_koin',create_paket_koin, name='create_paket_koin'),
    path('transaksi_paket_koin/<int:paket_koin>/<int:harga_paket>',transaksi_paket_koin,name='tranaksi_paket_koin'),
    path('delete_paket_koin/<int:paket_koin>',delete_paket_koin,name='delete_paket_koin'),
    path('update_paket_koin/<int:paket_koin>',update_paket_koin,name='update_paket_koin')
]