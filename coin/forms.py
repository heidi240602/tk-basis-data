from django import forms

class CreatePaketKoinForm(forms.Form):
    paket_koin=forms.IntegerField(
        label='Paket Koin',
        required=True
    )
    harga=forms.IntegerField(
        label="Harga Koin",
        required=True
    )
class CreateTransaksiPaketKoinForm(forms.Form):
    paket_koin=forms.IntegerField(
        label='Paket Koin',
        disabled=True
    )
    harga_paket=forms.IntegerField(
        label="Harga Paket",
        disabled=True
    )
    jumlah=forms.IntegerField(
        label='Jumlah yang Dibeli',
        required=True
    )
    cara_pembayaran=forms.CharField(
        label='Cara Pembayaran',
        required=True
    )
class DeletePaketKoinForm(forms.Form):
    paket_koin=forms.IntegerField(
        label="Nama Paket",
        disabled=True
    )
class UpdatePaketKoinForm(forms.Form):
    paket_koin=forms.IntegerField(
        label="Nama Paket",
        disabled=True
    )
    harga_baru=forms.IntegerField(
        label="Harga Baru Paket",
        required=True
    )
