from django.urls import path
from .views import create_produk, read_produk, update_produk, delete_produk

app_name = 'produk'
urlpatterns = [
    path('create', create_produk, name="create_produk"),
    path('read', read_produk, name="read_produk"),
    path('<produk>/update', update_produk, name="update_produk"),
    path('<produk>/delete', delete_produk, name="delete_produk")
]