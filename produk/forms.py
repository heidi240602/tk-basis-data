from django import forms

jenis_produk_choices = [
    ('HASIL_PANEN', 'Hasil Panen'),
    ('PRODUK_HEWAN', 'Produk Hewan'),
    ('PRODUK_MAKANAN', 'Produk Makanan'),
]

class CreateProdukForm(forms.Form):
    jenis_produk= forms.CharField(label='Jenis Produk', widget=forms.Select(choices=jenis_produk_choices))
    nama = forms.CharField(label='Nama', widget=forms.TextInput(attrs={'id': 'nama', 'placeholder': 'kulit sapi'}))
    harga_jual = forms.CharField(label='Harga Jual', widget=forms.NumberInput(attrs={'id': 'harga_jual', 'placeholder': '20000'}))
    sifat_produk = forms.CharField(label='Sifat Produk', widget=forms.TextInput(attrs={'id': 'sifat_produk'}), help_text="Raw Material / Finished Goods / Semi-Finished Goods")

class UpdateProdukForm(forms.Form):
    jenis_produk= forms.CharField(label='Jenis Produk', widget=forms.TextInput(attrs={'id': 'jenis_produk', 'readonly': True}))
    nama = forms.CharField(label='Nama', widget=forms.TextInput(attrs={'id': 'nama', 'readonly': True}))
    harga_jual = forms.CharField(label='Harga Jual',
                                 required=False,
                                 widget=forms.NumberInput(attrs={'id': 'harga_jual', 'required': True}))
    sifat_produk = forms.CharField(label='Sifat Produk',
                                   required=False,
                                   widget=forms.TextInput(attrs={'id': 'sifat_produk', 'required': True}), help_text="Raw Material / Finished Goods / Semi-Finished Goods")
