from django.db import connection
from django.shortcuts import render, redirect

# Create your views here.
from produk.forms import CreateProdukForm, UpdateProdukForm


def create_produk(request):
    if request.session.get("role", None) == None:
        return redirect("/authentication/user_login")
    if request.method == 'POST':
        form = CreateProdukForm(request.POST)
        if form.is_valid():
            with connection.cursor() as cursor:
                new_id = generate_id()
                jenis_produk = request.POST['jenis_produk']
                nama = request.POST['nama']
                harga_jual = request.POST['harga_jual']
                sifat_produk = request.POST['sifat_produk']
                cursor.execute("SET SEARCH_PATH TO HI_DAY")
                cursor.execute("INSERT INTO PRODUK (id, nama, harga_jual, sifat_produk) values (%s, %s, %s, %s);",
                               [new_id, nama, harga_jual, sifat_produk])
                cursor.execute("INSERT INTO " + jenis_produk + " VALUES ('" + new_id +"')")
            return redirect('/produk/read')
    form = CreateProdukForm()
    response = {"create_produk_form": form}
    return render(request, "create_produk.html", response)


def read_produk(request):
    if request.session.get("role", None) == None:
        return redirect("/authentication/user_login")
    generate_id()
    response = {}
    with connection.cursor() as cursor:
        cursor.execute("SET SEARCH_PATH TO HI_DAY")
        cursor.execute("""
                        SELECT CASE WHEN P.id IN (SELECT id_produk FROM PRODUK_MAKANAN) THEN 'Produk Makanan'
                        WHEN P.id IN (SELECT id_produk FROM PRODUK_HEWAN) THEN 'Produk Hewan'
                        ELSE 'Hasil Panen' END
                            AS jenis,
                        P.nama, P.harga_jual, P.sifat_produk, P.id
                        FROM PRODUK P order by id""")
        response['daftar_produk'] = cursor.fetchall()

        cursor.execute("""
                        SELECT CASE WHEN P.id IN (SELECT id_produk FROM PRODUK_MAKANAN) THEN 'Produk Makanan'
                        WHEN P.id IN (SELECT id_produk FROM PRODUK_HEWAN) THEN 'Produk Hewan'
                        ELSE 'Hasil Panen' END
                            AS jenis,
                        P.nama, P.harga_jual, P.sifat_produk, P.id FROM PRODUK P 
                        WHERE P.id NOT IN (SELECT DISTINCT id_produk FROM DETAIL_PESANAN) AND
                        P.id NOT IN (SELECT DISTINCT id_produk FROM LUMBUNG_MEMILIKI_PRODUK) AND
                        P.id NOT IN (SELECT DISTINCT id_produk_makanan FROM PRODUK_DIBUTUHKAN_OLEH_PRODUK_MAKANAN) AND
                        P.id NOT IN (SELECT DISTINCT id_produk FROM PRODUK_DIBUTUHKAN_OLEH_PRODUK_MAKANAN) AND
				        P.id NOT IN (SELECT DISTINCT id_hasil_panen FROM BIBIT_TANAMAN_MENGHASILKAN_HASIL_PANEN) AND
				        P.id NOT IN (SELECT DISTINCT id_hasil_panen FROM HEWAN_MEMBUTUHKAN_HASIL_PANEN) AND
				        P.id NOT IN (SELECT DISTINCT id_produk_makanan FROM PRODUKSI) AND
				        P.id NOT IN (SELECT DISTINCT id_produk_hewan FROM HEWAN_MENGHASILKAN_PRODUK_HEWAN);""")
        response['deletable'] = cursor.fetchall()
    if request.session.get("role", None) == "admin":
        return render(request, "read_produk_admin.html", response)
    else:
        return render(request, "read_produk.html", response)


def update_produk(request, produk):
    if request.session.get("role", None) == None:
        return redirect("/authentication/user_login")
    cursor = connection.cursor()
    if request.method == 'POST':
        form = UpdateProdukForm(request.POST)
        if form.is_valid():
            harga_jual = request.POST['harga_jual']
            sifat_produk = request.POST['sifat_produk']
            cursor.execute("SET SEARCH_PATH TO HI_DAY")
            cursor.execute("UPDATE PRODUK SET harga_jual = %s, sifat_produk = %s WHERE id = %s",
                           [harga_jual, sifat_produk, produk])
            return redirect('/produk/read')

    cursor.execute("SET SEARCH_PATH TO HI_DAY")
    cursor.execute("""SELECT CASE WHEN P.id IN (SELECT id_produk FROM PRODUK_MAKANAN) THEN 'Produk Makanan'
                        WHEN P.id IN (SELECT id_produk FROM PRODUK_HEWAN) THEN 'Produk Hewan'
                        ELSE 'Hasil Panen' END
                            AS jenis,
                        P.nama
                        FROM PRODUK P WHERE id = %s""", [produk])
    target = cursor.fetchone()
    data = {'jenis_produk': target[0], 'nama': target[1]}
    form = UpdateProdukForm(data)
    response = {"update_produk_form": form}
    return render(request, "update_produk.html", response)


def delete_produk(request, produk):
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO HI_DAY")
    cursor.execute("DELETE FROM PRODUK WHERE id = %s", [produk])
    return redirect('/produk/read')


def generate_id():
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO HI_DAY")
    cursor.execute("SELECT MAX(id) FROM PRODUK")
    last_id = cursor.fetchone()[0][1:]
    return "p" + str(int(last_id) + 1).zfill(3)
