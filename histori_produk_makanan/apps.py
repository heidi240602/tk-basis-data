from django.apps import AppConfig


class HistoriProdukMakananConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'histori_produk_makanan'
