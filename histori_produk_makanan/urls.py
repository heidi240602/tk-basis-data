from django.urls import path

from .views import create_histori_produksi_makanan, read_histori_produksi_makanan

app_name = 'histori_produksi_makanan'
urlpatterns = [
    path('create', create_histori_produksi_makanan, name='create_histori_produksi_makanan'),
    path('read', read_histori_produksi_makanan, name='read_histori_produksi_makanan')
]