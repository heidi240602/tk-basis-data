import datetime

from django import forms
from django.db import connection


def get_nama_produk_makanan():
    result = []
    with connection.cursor() as cursor:
        cursor.execute("SET SEARCH_PATH TO HI_DAY")
        cursor.execute("""
                        SELECT DISTINCT P.id, P.nama
                        FROM PRODUK_MAKANAN M LEFT OUTER JOIN PRODUK P
                        ON P.id = M.id_produk order by p.id asc;""")
        result = cursor.fetchall()
    return result


class CreateHistoriProduksiMakananForm(forms.Form):
    id_produk_makanan = forms.CharField(label='ID Produk Makanan',
                                        widget=forms.Select(choices=get_nama_produk_makanan()))
    jumlah = forms.CharField(label='Jumlah', widget=forms.NumberInput(attrs={'id': 'jumlah', 'placeholder': '2'}))
    xp = forms.CharField(label='XP',
                         widget=forms.TextInput(attrs={'id': 'xp', 'placeholder': '(disabled) x5 Jumlah', 'readonly': True}),
                         required=False)
