import datetime
from collections import namedtuple

from django.db import connection
from django.shortcuts import render, redirect

# Create your views here.
from histori_produk_makanan.forms import CreateHistoriProduksiMakananForm


def fetchall(cursor):
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]


def create_histori_produksi_makanan(request):
    if request.session.get("role", None) == None:
        return redirect("/authentication/user_login")
    response = {}
    if request.method == 'POST':
        form = CreateHistoriProduksiMakananForm(request.POST)
        if form.is_valid():
            id_produk_makanan = request.POST['id_produk_makanan']
            jumlah = request.POST['jumlah']
            waktu = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            if has_produk_for(id_produk_makanan, jumlah, request.session.get('email')):
                if has_alat_for(id_produk_makanan, request.session.get('email')):
                    cursor = connection.cursor()
                    id_alat = alat_yang_dibutuhkan(id_produk_makanan)
                    if len(id_alat) != 0:
                        cursor.execute('SET SEARCH_PATH TO HI_DAY;')
                        cursor.execute("INSERT INTO HISTORI_PRODUKSI values (%s, %s, %s, %s, %s);",
                                       [request.session.get('email'), waktu, waktu, jumlah, str(int(jumlah)*5)])
                        cursor.execute("INSERT INTO HISTORI_PRODUKSI_MAKANAN values (%s, %s, %s, %s);",
                                       [request.session.get('email'), waktu, id_alat[0][0], id_produk_makanan])
                        return redirect('/histori_produksi_makanan/read')
                    else:
                        response['error'] = 'Produk tidak membutuhkan alat (FK insertion failed, Produksi aborted)'
                else:
                    response['error'] = 'Anda tidak memiliki alat yang dibutuhkan'
            else:
                response['error'] = 'Anda tidak memiliki bahan yang cukup, silahkan menambahkan produk yang akan digunakan sebagai bahan terlebih dahulu'
    form = CreateHistoriProduksiMakananForm()
    response['create_histori_produksi_makanan_form'] = form
    return render(request, "create_histori_produksi_makanan.html", response)


def read_histori_produksi_makanan(request):
    if request.session.get("role", None) == None:
        return redirect("/authentication/user_login")
    response = {}
    with connection.cursor() as cursor:
        cursor.execute("SET SEARCH_PATH TO HI_DAY")
        if request.session.get("role", None) == "admin":
            cursor.execute("""
                        SELECT H.email, to_char(cast(H.waktu_awal as time), 'HH:MI:SS'), to_char(cast(H.waktu_selesai as time), 'HH:MI:SS'), H.jumlah, H.xp, P.nama, A.nama
                        FROM HISTORI_PRODUKSI_MAKANAN HM LEFT OUTER JOIN HISTORI_PRODUKSI H 
                        ON HM.email = H.email AND
                        HM.waktu_awal = H.waktu_awal
                        LEFT OUTER JOIN PRODUK P
                        ON HM.id_produk_makanan = P.ID
                        LEFT OUTER JOIN ASET A
                        ON HM.id_alat_produksi = A.ID order by HM.waktu_awal desc""")
            response['daftar_histori_produksi_makanan'] = cursor.fetchall()
        else:
            cursor.execute("""
                        SELECT to_char(cast(H.waktu_awal as time), 'HH:MI:SS'), to_char(cast(H.waktu_selesai as time), 'HH:MI:SS'), H.jumlah, H.xp, P.nama, A.nama
                        FROM HISTORI_PRODUKSI_MAKANAN HM LEFT OUTER JOIN HISTORI_PRODUKSI H 
                        ON HM.email = H.email AND
                        HM.waktu_awal = H.waktu_awal
                        LEFT OUTER JOIN PRODUK P
                        ON HM.id_produk_makanan = P.ID
                        LEFT OUTER JOIN ASET A
                        ON HM.id_alat_produksi = A.ID order by HM.waktu_awal desc""")
            response['daftar_histori_produksi_makanan'] = cursor.fetchall()

    if request.session.get("role", None) == "admin":
        return render(request, 'read_histori_produksi_makanan_admin.html', response)
    else:
        return render(request, 'read_histori_produksi_makanan.html', response)


def produk_yang_dibutuhkan(produk_makanan, jumlah):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO HI_DAY;')
    cursor.execute(
        'SELECT id_produk, (jumlah*%s)::VARCHAR(32) as jumlah FROM PRODUK_DIBUTUHKAN_OLEH_PRODUK_MAKANAN WHERE id_produk_makanan = %s;',
        [int(jumlah), produk_makanan])
    return fetchall(cursor)


def produk_yang_dimiliki(email):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO HI_DAY;')
    cursor.execute('SELECT id_produk, jumlah FROM LUMBUNG_MEMILIKI_PRODUK WHERE id_lumbung = %s', [email])
    return fetchall(cursor)


def has_produk_for(produk_makanan, jumlah, email):
    produk_dibutuhkan = produk_yang_dibutuhkan(produk_makanan, jumlah)
    produk_dimiliki = produk_yang_dimiliki(email)

    for dibutuhkan in produk_dibutuhkan:
        punya = False
        for dimiliki in produk_dimiliki:
            if dibutuhkan[0] == dimiliki[0] and int(dibutuhkan[1]) <= int(dimiliki[1]):
                punya = True
        if not punya:
            return False
    return True

def alat_yang_dibutuhkan(produk_makanan):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO HI_DAY;')
    cursor.execute('SELECT id_alat_produksi FROM PRODUKSI WHERE id_produk_makanan = %s;', [produk_makanan])
    return fetchall(cursor)

def alat_yang_dimiliki(email):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO HI_DAY;')
    cursor.execute('SELECT id_aset FROM KOLEKSI_ASET_MEMILIKI_ASET WHERE id_koleksi_aset = %s;', [email])
    return fetchall(cursor)

def has_alat_for(produk_makanan, email):
    alat_dibutuhkan = alat_yang_dibutuhkan(produk_makanan)
    alat_dimiliki = alat_yang_dimiliki(email)
    for dibutuhkan in alat_dibutuhkan:
        if dibutuhkan not in alat_dimiliki:
            return False
    return True