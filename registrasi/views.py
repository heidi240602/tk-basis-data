from collections import namedtuple

from django.db import connection
from django.shortcuts import render, redirect

# Create your views here.
from registrasi.forms import RegisterPenggunaForm, RegisterAdminForm


def homepage(request):
    if request.session.get("role", None) == None:
        return redirect("/authentication/user_login")
    response = {}
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO HI_DAY;")
    if request.session.get("role") == 'admin':
        cursor.execute("SELECT email FROM ADMIN WHERE email = %s;", [request.session.get('email')])
        response['biodata'] = cursor.fetchall()
    else:
        cursor.execute("""SELECT email, nama_area_pertanian, xp, koin, level
                        FROM PENGGUNA WHERE email = %s;""", [request.session.get('email')])
        response['biodata'] = cursor.fetchall()
    return render(request, 'homepage.html', response)


def fetchall(cursor):
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]


def register_admin(request):
    if "email" in request.session:
        return redirect('/')
    response = {'error': False}
    form = RegisterAdminForm(request.POST or None)
    response['form'] = form
    if request.method == 'POST' and form.is_valid():
        email = form.cleaned_data['email']
        password = form.cleaned_data['password']
        if not is_email_taken(email):
            cursor = connection.cursor()
            insert_akun(email)
            insert_admin(email, password)
            cursor.execute("SET SEARCH_PATH TO public")
            request.session['role'] = 'admin'
            request.session['email'] = email
            return redirect('/')
        else:
            response['error'] = True
    return render(request, 'RegistrasiAdmin.html', response)


def register_pengguna(request):
    if "email" in request.session:
        return redirect('/')
    response = {'error': False}
    form = RegisterPenggunaForm(request.POST or None)
    response['form'] = form
    if request.method == 'POST' and form.is_valid():
        email = form.cleaned_data['email']
        password = form.cleaned_data['password']
        nama_area_pertanian = form.cleaned_data['nama_area_pertanian']
        if not is_email_taken(email):
            cursor = connection.cursor()
            insert_akun(email)
            insert_pengguna(email, password, nama_area_pertanian)
            cursor.execute("SET SEARCH_PATH TO public")
            request.session['role'] = 'pengguna'
            request.session['email'] = email
            return redirect('/')
        else:
            response['error'] = True
    return render(request, 'RegistrasiPengguna.html', response)


def insert_pengguna(email, password, nama_area_pertanian):
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO HI_DAY;")
    cursor.execute("INSERT INTO pengguna (Email, Password, Nama_Area_Pertanian, XP, Koin, Level) values (%s,%s,%s,%s,"
                   "%s,%s)", [email, password, nama_area_pertanian, 0, 0, 1])


def insert_akun(email):
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO HI_DAY;")
    cursor.execute("INSERT INTO akun (email) VALUES (%s)", [email])


def insert_admin(email, password):
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO HI_DAY;")
    cursor.execute("INSERT INTO ADMIN (email, password) VALUES (%s,%s)", [email, password])


def is_email_taken(email):
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO HI_DAY;")
    cursor.execute("SELECT email FROM AKUN;")
    all_registered_email = fetchall(cursor)
    for registered_email in all_registered_email:
        if registered_email[0] == email:
            return True
    return False
