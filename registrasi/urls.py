from django.urls import path
from .views import homepage, register_pengguna, register_admin

urlpatterns = [
    path('', homepage, name="homepage"),
    path('register_pengguna', register_pengguna, name="register_pengguna"),
    path('register_admin', register_admin, name="register_admin")
]
