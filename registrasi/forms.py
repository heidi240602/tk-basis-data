from django import forms


class LoginForm(forms.Form):
    username = forms.CharField(label='Username', max_length=50)
    password = forms.CharField(label='Password', max_length=50)

class RegisterAdminForm(forms.Form):
    email=forms.CharField(label='Email',max_length=50)
    password=forms.CharField(label='Password',max_length=20, widget=forms.PasswordInput)

class RegisterPenggunaForm(forms.Form):
    email=forms.EmailField(label='Email', max_length=50)
    password=forms.CharField(label='Password', max_length=50, widget=forms.PasswordInput)
    nama_area_pertanian=forms.CharField(label='Nama Area Pertanian', max_length=50)