import email
from urllib import request
from django.shortcuts import render, redirect
from django.db import connection
from .forms import LoginForm

# Create your views here.

def is_registered(email:str) -> bool:
    path="SET SEARCH_PATH TO HI_DAY"
    command="SELECT * FROM AKUN"
    with connection.cursor() as cursor:
        cursor.execute(path)
        cursor.execute(command)
        data_akun=cursor.fetchall()
    for data in data_akun:
        if data[0]==email:
            return True
    return False


def role(email:str, password:str) -> str:
    path="SET SEARCH_PATH TO HI_DAY"
    command="SELECT * FROM PENGGUNA"
    command2="SELECT * FROM ADMIN"
    with connection.cursor() as cursor:
        cursor.execute(path)
        cursor.execute(command)
        data_pengguna=cursor.fetchall()

    with connection.cursor() as cursor:
        cursor.execute(path)
        cursor.execute(command2)
        data_admin=cursor.fetchall()
    for data in data_pengguna:
        if data[0]==email and data[1]==password:
            return "pengguna"
        elif data[0]==email and data[1]!=password:
            return "invalid password"
    for data in data_admin:
        if data[0]==email and data[1]==password:
            return "admin"
        elif data[0]==email and data[1]!=password:
            return "invalid password"
    return "unexpected error"

def user_login(request):
    cursor=connection.cursor()
    if 'email' in request.session:
        redirect('/')

    response={}
    cursor = connection.cursor()
    form=LoginForm(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):

        response['email']=request.POST['email']
        response['password']=request.POST['password']
        response['error_msg']= ""

        registered=is_registered(response['email'])
        if not registered:
            response['error_msg']="email belum terdaftar"
        else:
            acc_role= role(response['email'],response['password'])
            if acc_role != "admin" and acc_role!="pengguna":
                response['error_msg']="role tidak diketahui"

            else:
                cursor.execute("SET search_path TO public")
                request.session['role']=acc_role
                request.session['email']=response['email']
                return redirect('/')
    response['form']=form
    return render(request,'login.html',context=response)

def user_logout(request):
    if "email" in request.session:
        request.session.flush()
        return redirect('/authentication/user_login')
    return redirect('/authentication/user_login')
        






