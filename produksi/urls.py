from django.urls import path

from .views import create_produksi, read_produksi, update_produksi, read_produksi_detail, delete_produksi

app_name = 'produksi'
urlpatterns = [
    path('create', create_produksi, name='create_level'),
    path('read', read_produksi, name='read_level'),
    path('<id_alat_produksi>/<id_produk_makanan>/update', update_produksi, name='update_produksi'),
    path('<id_alat_produksi>/<id_produk_makanan>/detail', read_produksi_detail, name='read_produksi_detail'),
    path('<id_alat_produksi>/<id_produk_makanan>/delete', delete_produksi, name='delete_produksi')
]