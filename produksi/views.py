from django.db import connection
from django.shortcuts import render, redirect

# Create your views here.
from produksi.forms import CreateProduksiForm, UpdateProduksiForm


def create_produksi(request):
    if request.session.get("role", None) == None:
        return redirect("/authentication/user_login")
    cursor = connection.cursor()
    response = {}
    cursor.execute("SET SEARCH_PATH TO HI_DAY")
    cursor.execute("SELECT id_produk, nama FROM PRODUK_MAKANAN LEFT OUTER JOIN PRODUK ON id=id_produk WHERE id_produk not in (SELECT id_produk_makanan FROM PRODUKSI);")
    response['nama_produk_makanan'] = cursor.fetchall()
    cursor.execute("SELECT id_aset, nama FROM ALAT_PRODUKSI LEFT OUTER JOIN ASET ON id = id_aset;")
    response['alat_produksi'] = cursor.fetchall()
    cursor.execute("SELECT id, nama FROM PRODUK;")
    response['pilihan_bahan'] = cursor.fetchall()
    form = CreateProduksiForm()
    response['create_produksi_form'] = form
    print("view called ...")
    if request.method == 'POST':
        print("request is post ...")
        if (request.POST['id_produk_makanan'] != "") and (request.POST['id_alat_produksi'] != "") and (request.POST['durasi'] != "") and (request.POST['jumlah'] != "") and ("" not in request.POST.getlist('bahan') != 0) and ("" not in request.POST.getlist('jumlah_bahan')):
            print("form is valid ...")
            id_produk_makanan = request.POST['id_produk_makanan']
            id_alat_produksi = request.POST['id_alat_produksi']
            durasi_dalam_menit = int(request.POST['durasi'])
            jumlah = request.POST['jumlah']
            hours = str(int(durasi_dalam_menit / 60)).zfill(2)
            minutes = str(int(durasi_dalam_menit % 60)).zfill(2)
            durasi = "%s:%s:00" % (hours, minutes)
            cursor.execute("SET SEARCH_PATH TO HI_DAY")
            cursor.execute("INSERT INTO PRODUKSI VALUES (%s, %s, %s, %s)",[id_alat_produksi, id_produk_makanan, durasi, jumlah])
            list_bahan = request.POST.getlist('bahan')
            jumlah_bahan = request.POST.getlist('jumlah_bahan')
            for i in range(len(list_bahan)):
                cursor.execute("INSERT INTO PRODUK_DIBUTUHKAN_OLEH_PRODUK_MAKANAN VALUES (%s, %s, %s)", [id_produk_makanan, list_bahan[i], jumlah_bahan[i]])
            return redirect('/produksi/read')
        else:
            print("form is not valid ...")
            response['error'] = 'Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu'
            return render(request, "create_produksi.html", response)
    return render(request, "create_produksi.html", response)


def read_produksi(request):
    if request.session.get("role", None) == None:
        return redirect("/authentication/user_login")
    response = {}
    with connection.cursor() as cursor:
        cursor.execute("SET SEARCH_PATH TO HI_DAY")
        cursor.execute("""SELECT M.nama, A.nama, trunc(extract(epoch from (P.durasi - '00:00:00')) / 60) as durasi, P.jumlah_unit_hasil, A.id, M.id
                       FROM PRODUKSI P, PRODUK M, ASET A
                       WHERE M.ID = P.ID_Produk_Makanan
                       AND A.ID = P.ID_Alat_Produksi""")
        response["daftar_produksi"] = cursor.fetchall()

        cursor.execute("""SELECT M.nama, A.nama, trunc(extract(epoch from (P.durasi - '00:00:00')) / 60) as durasi, P.jumlah_unit_hasil, A.id, M.id
                       FROM PRODUKSI P, PRODUK M, ASET A
                       WHERE M.ID = P.ID_Produk_Makanan
                       AND A.ID = P.ID_Alat_Produksi
                       AND (A.ID, M.ID) not in (
                       SELECT id_alat_produksi, id_produk_makanan FROM HISTORI_PRODUKSI_MAKANAN)""")
        response["deletable"] = cursor.fetchall()

    if request.session.get("role", None) == "admin":
        return render(request, "read_produksi_admin.html", response)
    else:
        return render(request, "read_produksi.html", response)


def read_produksi_detail(request, id_alat_produksi, id_produk_makanan):
    if request.session.get("role", None) == None:
        return redirect("/authentication/user_login")
    response = {}
    with connection.cursor() as cursor:
        cursor.execute("SET SEARCH_PATH TO HI_DAY")
        cursor.execute("""SELECT M.nama, A.nama, trunc(extract(epoch from (P.durasi - '00:00:00')) / 60) as durasi, P.jumlah_unit_hasil
                       FROM PRODUKSI P, PRODUK M, ASET A
                       WHERE M.ID = P.ID_Produk_Makanan
                       AND A.ID = P.ID_Alat_Produksi
                       AND P.ID_Alat_Produksi = %s
                       AND P.ID_Produk_Makanan = %s""", [id_alat_produksi, id_produk_makanan])
        response["detail_produksi"] = cursor.fetchone()
        cursor.execute("""SELECT P.nama, PD.jumlah 
                            FROM PRODUK_DIBUTUHKAN_OLEH_PRODUK_MAKANAN PD LEFT OUTER JOIN PRODUK P 
                            ON PD.id_produk = P.id
                            WHERE PD.id_produk_makanan = %s;""", [id_produk_makanan])
        response["membutuhkan_produk"] = cursor.fetchall()
    return render(request, "read_produksi_detail.html", response)


def update_produksi(request, id_alat_produksi, id_produk_makanan):
    if request.session.get("role", None) == None:
        return redirect("/authentication/user_login")
    response = {}
    cursor = connection.cursor()
    if request.method == 'POST':
        form = UpdateProduksiForm(request.POST)
        if form.is_valid():
            durasi_dalam_menit = int(request.POST['durasi'])
            hours = str(int(durasi_dalam_menit / 60)).zfill(2)
            minutes = str(int(durasi_dalam_menit % 60)).zfill(2)
            durasi = "%s:%s:00" % (hours, minutes)
            jumlah = request.POST['jumlah_yang_dihasilkan']
            cursor.execute("SET SEARCH_PATH TO HI_DAY")
            cursor.execute("""UPDATE PRODUKSI SET durasi = %s, jumlah_unit_hasil = %s 
                           WHERE id_alat_produksi = %s AND id_produk_makanan = %s""",
                           [durasi, jumlah, id_alat_produksi, id_produk_makanan])
            return redirect('/produksi/read')

    cursor.execute("SET SEARCH_PATH TO HI_DAY")
    cursor.execute("""SELECT M.nama, A.nama
                   FROM PRODUKSI P, PRODUK M, ASET A
                   WHERE M.ID = P.ID_Produk_Makanan
                   AND A.ID = P.ID_Alat_Produksi
                   AND P.ID_Alat_Produksi = %s
                   AND P.ID_Produk_Makanan = %s""", [id_alat_produksi, id_produk_makanan])
    target = cursor.fetchone()
    data = {'nama_produk_makanan': target[0], 'alat_produksi': target[1]}
    cursor.execute("""SELECT P.nama, PD.jumlah 
                        FROM PRODUK_DIBUTUHKAN_OLEH_PRODUK_MAKANAN PD LEFT OUTER JOIN PRODUK P 
                        ON PD.id_produk = P.id
                        WHERE PD.id_produk_makanan = %s;""", [id_produk_makanan])
    response["membutuhkan_produk"] = cursor.fetchall()
    form = UpdateProduksiForm(data)
    response["update_produksi_form"] = form
    return render(request, "update_produksi.html", response)


def delete_produksi(request, id_alat_produksi, id_produk_makanan):
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO HI_DAY")
    cursor.execute("DELETE FROM PRODUKSI WHERE id_alat_produksi = %s AND id_produk_makanan = %s", [id_alat_produksi, id_produk_makanan])
    cursor.execute("DELETE FROM PRODUK_DIBUTUHKAN_OLEH_PRODUK_MAKANAN WHERE id_produk_makanan = %s", [id_produk_makanan])
    return redirect('/produksi/read')
