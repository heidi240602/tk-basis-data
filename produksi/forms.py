from django import forms
from django.db import connection


def get_nama_produk_makanan():
    result = []
    with connection.cursor() as cursor:
        cursor.execute("SET SEARCH_PATH TO HI_DAY")
        cursor.execute("""
                        SELECT DISTINCT P.nama, P.nama
                        FROM PRODUK_MAKANAN M LEFT OUTER JOIN PRODUK P
                        ON P.id = M.id_produk""")
        result = cursor.fetchall()
    return result


def get_nama_alat_produksi():
    result = []
    with connection.cursor() as cursor:
        cursor.execute("SET SEARCH_PATH TO HI_DAY")
        cursor.execute("""
                        SELECT DISTINCT A.nama, A.nama
                        FROM ALAT_PRODUKSI P LEFT OUTER JOIN ASET A
                        ON A.id = P.id_aset""")
        result = cursor.fetchall()
    return result


def get_nama_produk():
    result = []
    with connection.cursor() as cursor:
        cursor.execute("SET SEARCH_PATH TO HI_DAY")
        cursor.execute("SELECT DISTINCT nama, nama FROM PRODUK")
        result = cursor.fetchall()
    return result


class CreateProduksiForm(forms.Form):
    nama_produk_makanan = forms.CharField(label='Nama Produk Makanan')
    alat_produksi = forms.CharField(label='Alat Produksi')
    durasi = forms.CharField(label='Durasi Produksi (dalam menit)')
    jumlah_yang_dihasilkan = forms.CharField(label='Jumlah Produk yang Dihasilkan')
    bahan = forms.CharField(label='Bahan')
    jumlah_bahan = forms.CharField(label='Jumlah')


class UpdateProduksiForm(forms.Form):
    nama_produk_makanan = forms.CharField(label='Nama Produk Makanan',
                                          widget=forms.TextInput(attrs={'id': 'nama_produk_makanan', 'readonly': True}))
    alat_produksi = forms.CharField(label='Alat Produksi',
                                    widget=forms.TextInput(attrs={'id': 'alat_produksi', 'readonly': True}))
    durasi = forms.CharField(label='Durasi Produksi (dalam menit)',
                             required=False,
                             widget=forms.TextInput(attrs={'id': 'durasi', 'required': True, 'type': 'number'}))
    jumlah_yang_dihasilkan = forms.CharField(label='Jumlah Produk yang Dihasilkan',
                                             required=False,
                                             widget=forms.TextInput(attrs={'id': 'jumlah_yang_dihasilkan', 'required': True, 'type': 'number'}))
