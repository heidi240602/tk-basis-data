from django.urls import path

from .views import *

app_name = 'lumbung'
urlpatterns = [
    path('read', read_lumbung, name='read_lumbung')
]