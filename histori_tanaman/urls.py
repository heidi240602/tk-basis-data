from django.urls import path
from .views import histori_tanaman,create_histori_tanaman

urlpatterns = [
    path('histori_tanaman',histori_tanaman, name='histori_tanaman'),
    path('create_histori_tanaman',create_histori_tanaman, name='create'),
]