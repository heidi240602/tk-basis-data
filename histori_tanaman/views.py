from django.shortcuts import render
import datetime
from time import strftime
from django.shortcuts import redirect, render
from django.db import connection

def histori_tanaman(request):
    response={}
    if('email' in request.session)!= True:
        return redirect('/authentication/user_login')
    if request.session['role']!='pengguna':
        email="'"+request.session['email']+"'"
        sql_command="SET SEARCH_PATH TO HI_DAY"
        sql_command2="SELECT* FROM HISTORI_TANAMAN"
        with connection.cursor() as cursor:
            cursor.execute(sql_command)
            cursor.execute(sql_command2)
            response["histori_tanaman"]=cursor.fetchall()
        return render(request,'histori_tanaman_admin.html',response)
    email="'"+request.session['email']+"'"
    sql_command="SET SEARCH_PATH TO HI_DAY"
    sql_command2="SELECT* FROM HISTORI_TANAMAN WHERE EMAIL="+email
    with connection.cursor() as cursor:
        cursor.execute(sql_command)
        cursor.execute(sql_command2)
        response["histori_tanaman"]=cursor.fetchall()
    return render(request,'histori_tanaman.html',response)

def create_histori_tanaman(request):
    response={}
    response['erro_msg']=""
    if('email' in request.session)!=True:
        return redirect("authentication/user_login")
    if request.session['role']!='pengguna':
        return redirect("/histori_tanaman")
    if request.method=='POST':
        email="'"+request.session['email']+"'"
        id_bibit=request.POST['bibit_tanaman']
        jumlah_bibit=int(request.POST['jumlah'])
        XP=int(jumlah_bibit)*5
        waktu="'"+str(datetime.datetime.now())+"'"

        jumlah_bibit_now=int(get_jumlah(request.session['email'],id_bibit))
        if jumlah_bibit > jumlah_bibit_now :
            response['bibit_tanaman']=get_bibit(request.session['email'],id_bibit)
            response['error_msg']="Anda tidak memiliki bibit yang cukup, silahkan membeli bibit terlebih dahulu"
            return render(request,'create_histori_tanaman.html',response)
        sql_command="SET SEARCH_PATH TO HI_DAY"
        sql_command2="INSERT INTO HISTORI_PRODUKSI(EMAIL, WAKTU_AWAL,WAKTU_SELESAI,JUMLAH,XP) VALUES({},{},{},{},{})".format(email,waktu,waktu,jumlah_bibit,jumlah_bibit*5)
        sql_command3="INSERT INTO HISTORI_TANAMAN(EMAIL,WAKTU_AWAL,ID_BIBIT_TANAMAN) VALUES({},{},{})".format(email,waktu,"'"+id_bibit+"'")
        with connection.cursor() as cursor:
            cursor.execute(sql_command)
            cursor.execute(sql_command2)
            cursor.execute(sql_command3)
        return redirect("/histori_produk_tanaman/histori_tanaman")
    response['bibit_tanaman']=get_bibit(request.session['email'])
    return render(request,'create_histori_tanaman.html',response)
    

def get_bibit(email):
    email="'"+email+"'"
    with connection.cursor() as cursor:
        cursor.execute("SET SEARCH_PATH TO HI_DAY")
        cursor.execute("SELECT DISTINCT * FROM ASET A, KOLEKSI_ASET_MEMILIKI_ASET KMA WHERE A.ID=KMA.ID_ASET AND KMA.ID_KOLEKSI_ASET="+str(email)+"AND EXISTS(SELECT BT.ID_ASET FROM BIBIT_TANAMAN AS BT WHERE BT.ID_ASET=A.ID)")
        data=cursor.fetchall()
    return data

def get_jumlah(email, id_aset):
    email="'"+email+"'"
    with connection.cursor() as cursor:
        cursor.execute("SET SEARCH_PATH TO HI_DAY")
        cursor.execute("SELECT KMA.JUMLAH FROM KOLEKSI_ASET_MEMILIKI_ASET KMA WHERE KMA.ID_ASET="+"'"+str(id_aset)+"'"+"AND KMA.ID_KOLEKSI_ASET="+str(email))
        print("SELECT KMA.JUMLAH FROM KOLEKSI_ASET_MEMILIKI_ASET KMA WHERE KMA.ID_ASET="+"'"+str(id_aset)+"'"+"AND KMA.ID_KOLEKSI_ASET="+str(email))
        data=cursor.fetchall()
    return data[0][0]
    